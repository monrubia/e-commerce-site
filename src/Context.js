import React, { Component } from 'react';
import {storeProducts, detailProduct} from './data';

const ProductContext= React.createContext();

class ProductProvider extends Component {
    state = {
        products: [],
        detailProduct : detailProduct,
        cart: [],
        modalOpen: false,
        modalProduct: detailProduct,
        cartSubtotal:0,
        cartTax: 0,
        cartTotal: 0
    };
    componentDidMount(){
        //console.log('component DID mount');
        this.setProducts();
    }
    setProducts = () => {
        //console.log('setProducts called');
        let tempProducts = [];
        storeProducts.forEach(item =>{
            const singleItem = {...item};
            tempProducts = [...tempProducts, singleItem];
        })
        this.setState(()=>{
            return {products: tempProducts}
        })
    };
    getItem = (id) =>{
        const product = this.state.products.find(item => item.id===id);
        return product;
    };
    handleDetail = (id) =>{
        //console.log(`handleDetail launched`);
        const product = this.getItem(id);
        this.setState(()=>{
            return{ detailProduct:product }
        })
        //console.log(`detailProduct defined: ${detailProduct.title}`);
    };
    addToCart = (id) =>{
        let tempProducts =[...this.state.products];
        const index = tempProducts.indexOf(this.getItem(id));
        const product = tempProducts[index];
        product.inCart = true;
        product.count = 1;
        const price = product.price;
        product.total = price;
        this.setState(() =>{
            return{
                products:tempProducts,
                cart: [...this.state.cart, product],
                detailProduct: {...product}
            };
        }, () =>{
            this.addTotals();
        })
    };
    openModal = (id) =>{
        const product =this.getItem(id);
        this.setState(()=>{
            return {
                modalProduct: product,
                modalOpen: true
            }
        });
    };
    closeModal = () =>{
        this.setState(()=>{
            return {modalOpen: false};
        });
    };
    //modif = string plus or minus
    modifAmount = (modif, id) =>{
        //console.log(`modif method: ${modif}`);
        let tempCart = [...this.state.cart];

        const selectedProduct = tempCart.find(item => item.id === id);
        const index = tempCart.indexOf(selectedProduct);
        const productToModif = tempCart[index];
        if (modif === 'plus') {
            //increase amount
            productToModif.count =  productToModif.count + 1;
        } else {
            //decrease amount
            productToModif.count =  productToModif.count - 1;
        }
        //once modified, delete or update
        if (productToModif.count === 0){
            this.removeItem(id);
        } else {
            productToModif.total =  productToModif.price * productToModif.count; 
            this.setState(() =>{
                return{
                    cart: [...tempCart]
                };
            }, () => { 
                this.addTotals();
            }
            )
        };
    };
    removeItem = (id) =>{
        let tempCart = [...this.state.cart];
        let tempProducts = [...this.state.products];

        tempCart = tempCart.filter(item => item.id !== id);

        const index = tempProducts.indexOf(this.getItem(id));
        let removedProduct = tempProducts[index];
        removedProduct.inCart = false;
        removedProduct.count = 0;
        removedProduct.total = 0;
        this.setState(() => {
            return {
                cart: [...tempCart],
                products: [...tempProducts]
            };
        }, () => { this.addTotals();}
        );
    };
    clearCart = () =>{
        this.setState(() =>{
            return { cart: [] };
        }, () =>{
            this.setProducts();
            this.addTotals();
        })
    };
    addTotals = () =>{
        //console.log(`Cart state when calling addTotals ${this.state.cart}`);
        let total = 0;
        this.state.cart.map(item => (total += item.total))
        const tempTax = total *0.2;
        const tax = parseFloat(tempTax.toFixed(2))
        this.setState(() =>{
            return{
                cartNoTax: total-tax,
                cartTax: tax,
                cartTotal: total
            }
        })
    };

    render() {
        return (
            <ProductContext.Provider 
                value={{
                    ...this.state,
                    handleDetail: this.handleDetail,
                    addToCart: this.addToCart,
                    openModal: this.openModal,
                    closeModal: this.closeModal,
                    modifAmount: this.modifAmount,
                    removeItem: this.removeItem,
                    clearCart: this.clearCart,
                }}
            >
                {this.props.children}
            </ProductContext.Provider>
        )
    }
}

const ProductConsumer = ProductContext.Consumer;

export {ProductContext, ProductProvider, ProductConsumer};