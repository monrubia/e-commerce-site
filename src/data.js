export const storeProducts = [
	{
		id: 1,
		title: "Couple with a message",
		img: "img/couple.jpg",
		img_carousel: "img/couple_carousel.jpg",
		price: 100,
		artist: "MDB",
		year: 2012,
		info:
			"Lorem ipsum dolor amet offal butcher quinoa sustainable gastropub, echo park actually green juice sriracha paleo. Brooklyn sriracha semiotics, DIY coloring book mixtape craft beer sartorial hella blue bottle. Tote bag wolf authentic try-hard put a bird on it mumblecore. Unicorn lumbersexual master cleanse blog hella VHS, vaporware sartorial church-key cardigan single-origin coffee lo-fi organic asymmetrical. Taxidermy semiotics celiac stumptown scenester normcore, ethical helvetica photo booth gentrify.",
		tags: ["deco", "rust", "MDB"], 
		inCart: false,
		inCarousel: true,
		count: 0,
		total: 0
	},
	{
		id: 2,
		title: "Bowl Violet-broken white",
		img: "img/bowl2.jpg",
		price: 100,
		artist: "Emma",
		year: 2020,
		info:
			"Lorem ipsum dolor amet offal butcher quinoa sustainable gastropub, echo park actually green juice sriracha paleo. Brooklyn sriracha semiotics, DIY coloring book mixtape craft beer sartorial hella blue bottle. Tote bag wolf authentic try-hard put a bird on it mumblecore. Unicorn lumbersexual master cleanse blog hella VHS, vaporware sartorial church-key cardigan single-origin coffee lo-fi organic asymmetrical. Taxidermy semiotics celiac stumptown scenester normcore, ethical helvetica photo booth gentrify.",
		tags: ["bowl", "purple", "white", "medium", "Emma"], 
		inCart: false,
		inCarousel: false,
		count: 0,
		total: 0
	},
	{
		id: 3,
		title: "Small  green bowl",
		img: "img/bowl3.jpg",
		price: 50,
		artist: "MOI",
		year: 2020,
		info:
			"Lorem ipsum dolor amet offal butcher quinoa sustainable gastropub, echo park actually green juice sriracha paleo. Brooklyn sriracha semiotics, DIY coloring book mixtape craft beer sartorial hella blue bottle. Tote bag wolf authentic try-hard put a bird on it mumblecore. Unicorn lumbersexual master cleanse blog hella VHS, vaporware sartorial church-key cardigan single-origin coffee lo-fi organic asymmetrical. Taxidermy semiotics celiac stumptown scenester normcore, ethical helvetica photo booth gentrify.",
		tags: ["bowl", "green", "small", "MOI"], 
		inCart: false,
		inCarousel: false,
		count: 0,
		total: 0
	},
	{
		id: 4,
		title: "Bowl spring green deep blue",
		img: "img/bowl4.jpg",
		img_carousel: "img/bowl4_carousel.jpg",
		price: 75,
		artist: "Marta",
		year: 2020,
		info:
			"Lorem ipsum dolor amet offal butcher quinoa sustainable gastropub, echo park actually green juice sriracha paleo. Brooklyn sriracha semiotics, DIY coloring book mixtape craft beer sartorial hella blue bottle. Tote bag wolf authentic try-hard put a bird on it mumblecore. Unicorn lumbersexual master cleanse blog hella VHS, vaporware sartorial church-key cardigan single-origin coffee lo-fi organic asymmetrical. Taxidermy semiotics celiac stumptown scenester normcore, ethical helvetica photo booth gentrify.",
		tags: ["bowl", "green", "blue", "medium", "Marta"], 
		inCart: false,
		inCarousel: true,
		count: 0,
		total: 0
	},
	{
		id: 5,
		title: "Deep blue Butterfly",
		img: "img/butterfly.jpg",
		price: 50,
		artist: "Marta",
		year: 2020,
		info:
			"Lorem ipsum dolor amet offal butcher quinoa sustainable gastropub, echo park actually green juice sriracha paleo. Brooklyn sriracha semiotics, DIY coloring book mixtape craft beer sartorial hella blue bottle. Tote bag wolf authentic try-hard put a bird on it mumblecore. Unicorn lumbersexual master cleanse blog hella VHS, vaporware sartorial church-key cardigan single-origin coffee lo-fi organic asymmetrical. Taxidermy semiotics celiac stumptown scenester normcore, ethical helvetica photo booth gentrify.",
		tags: ["deco", "blue", "Marta"], 
		inCart: false,
		inCarousel: false,
		count: 0,
		total: 0
	},
	{
		id: 6,
		title: "Coffee set",
		img: "img/coffee.jpg",
		img_carousel: "img/coffee_carousel.jpg",
		price: 100,
		artist: "MDB",
		year: 2019,
		info:
			"Lorem ipsum dolor amet offal butcher quinoa sustainable gastropub, echo park actually green juice sriracha paleo. Brooklyn sriracha semiotics, DIY coloring book mixtape craft beer sartorial hella blue bottle. Tote bag wolf authentic try-hard put a bird on it mumblecore. Unicorn lumbersexual master cleanse blog hella VHS, vaporware sartorial church-key cardigan single-origin coffee lo-fi organic asymmetrical. Taxidermy semiotics celiac stumptown scenester normcore, ethical helvetica photo booth gentrify.",
		tags: ["kitchen", "mug", "black", "vanilla", "MDB"], 
		inCart: false,
		inCarousel: true,
		count: 0,
		total: 0
	},
	{
		id: 7,
		title: "Autumn",
		img: "img/autunm_candle.jpg",
		img_carousel: "img/autunm_candle_night.jpg",
		price: 75,
		artist: "MOI",
		year: 2018,
		info:
			"Lorem ipsum dolor amet offal butcher quinoa sustainable gastropub, echo park actually green juice sriracha paleo. Brooklyn sriracha semiotics, DIY coloring book mixtape craft beer sartorial hella blue bottle. Tote bag wolf authentic try-hard put a bird on it mumblecore. Unicorn lumbersexual master cleanse blog hella VHS, vaporware sartorial church-key cardigan single-origin coffee lo-fi organic asymmetrical. Taxidermy semiotics celiac stumptown scenester normcore, ethical helvetica photo booth gentrify.",
		tags: ["candle", "deco", "white", "orange", "MOI"], 
		inCart: false,
		inCarousel: false,
		count: 0,
		total: 0
	},
	{
		id: 8,
		title: "Andalusi candle",
		img: "img/starts_candle_night.jpg",
		price: 75,
		artist: "MOI",
		year: 2018,
		info:
			"Lorem ipsum dolor amet offal butcher quinoa sustainable gastropub, echo park actually green juice sriracha paleo. Brooklyn sriracha semiotics, DIY coloring book mixtape craft beer sartorial hella blue bottle. Tote bag wolf authentic try-hard put a bird on it mumblecore. Unicorn lumbersexual master cleanse blog hella VHS, vaporware sartorial church-key cardigan single-origin coffee lo-fi organic asymmetrical. Taxidermy semiotics celiac stumptown scenester normcore, ethical helvetica photo booth gentrify.",
		tags: ["candle", "deco", "white", "MOI"], 
		inCart: false,
		inCarousel: false,
		count: 0,
		total: 0
	},
	{
		id: 9,
		title: "Palette dish",
		img: "img/painting_dish.jpg",
		price: 50,
		artist: "MOI",
		year: 2018,
		info:
			"Dish made to present a palette of colorful purées.",
		tags: ["kitchen", "dish", "white", "MOI"], 
		inCart: false,
		inCarousel: false,
		count: 0,
		total: 0
	},
	{
		id: 10,
		title: "Bowl green frog deep blue",
		img: "img/bowl1.jpg",
		price: 75,
		artist: "Marta",
		year: 2020,
		info:
			"Lorem ipsum dolor amet offal butcher quinoa sustainable gastropub, echo park actually green juice sriracha paleo. Brooklyn sriracha semiotics, DIY coloring book mixtape craft beer sartorial hella blue bottle. Tote bag wolf authentic try-hard put a bird on it mumblecore. Unicorn lumbersexual master cleanse blog hella VHS, vaporware sartorial church-key cardigan single-origin coffee lo-fi organic asymmetrical. Taxidermy semiotics celiac stumptown scenester normcore, ethical helvetica photo booth gentrify.",
		tags: ["bowl", "green", "blue", "medium", "Marta"], 
		inCart: false,
		inCarousel: false,
		count: 0,
		total: 0
	},
	{
		id: 11,
		title: "Spring bowl",
		img: "img/bowl5.jpg",
		price: 50,
		artist: "Marta",
		year: 2019,
		info:
			"Lorem ipsum dolor amet offal butcher quinoa sustainable gastropub, echo park actually green juice sriracha paleo. Brooklyn sriracha semiotics, DIY coloring book mixtape craft beer sartorial hella blue bottle. Tote bag wolf authentic try-hard put a bird on it mumblecore. Unicorn lumbersexual master cleanse blog hella VHS, vaporware sartorial church-key cardigan single-origin coffee lo-fi organic asymmetrical. Taxidermy semiotics celiac stumptown scenester normcore, ethical helvetica photo booth gentrify.",
		tags: ["bowl", "yellow", "orange","purple", "medium", "Marta"], 
		inCart: false,
		inCarousel: false,
		count: 0,
		total: 0
	},
	{
		id: 12,
		title: "Saint-Jacques dish",
		img: "img/dish1.jpg",
		price: 50,
		artist: "MDB",
		year: 2018,
		info:
			"Lorem ipsum dolor amet offal butcher quinoa sustainable gastropub, echo park actually green juice sriracha paleo. Brooklyn sriracha semiotics, DIY coloring book mixtape craft beer sartorial hella blue bottle. Tote bag wolf authentic try-hard put a bird on it mumblecore. Unicorn lumbersexual master cleanse blog hella VHS, vaporware sartorial church-key cardigan single-origin coffee lo-fi organic asymmetrical. Taxidermy semiotics celiac stumptown scenester normcore, ethical helvetica photo booth gentrify.",
		tags: ["kitchen", "dish", "vanilla", "MDB"], 
		inCart: false,
		inCarousel: false,
		count: 0,
		total: 0
	},
	{
		id: 13,
		title: "The egg Bowl",
		img: "img/bowl6.jpg",
		price: 10,
		artist: "Marta",
		year: 2018,
		info:
			"Lorem ipsum dolor amet offal butcher quinoa sustainable gastropub, echo park actually green juice sriracha paleo. Brooklyn sriracha semiotics, DIY coloring book mixtape craft beer sartorial hella blue bottle. Tote bag wolf authentic try-hard put a bird on it mumblecore. Unicorn lumbersexual master cleanse blog hella VHS, vaporware sartorial church-key cardigan single-origin coffee lo-fi organic asymmetrical. Taxidermy semiotics celiac stumptown scenester normcore, ethical helvetica photo booth gentrify.",
		tags: ["bowl", "white", "yellow", "medium", "Marta"], 
		inCart: false,
		inCarousel: false,
		count: 0,
		total: 0
	},
	{
		id: 14,
		title: "Sunny day Bowl",
		img: "img/bowl7.jpg",
		price: 10,
		artist: "Marta",
		year: 2018,
		info:
			"Lorem ipsum dolor amet offal butcher quinoa sustainable gastropub, echo park actually green juice sriracha paleo. Brooklyn sriracha semiotics, DIY coloring book mixtape craft beer sartorial hella blue bottle. Tote bag wolf authentic try-hard put a bird on it mumblecore. Unicorn lumbersexual master cleanse blog hella VHS, vaporware sartorial church-key cardigan single-origin coffee lo-fi organic asymmetrical. Taxidermy semiotics celiac stumptown scenester normcore, ethical helvetica photo booth gentrify.",
		tags: ["bowl", "ocean", "yellow", "medium", "Marta"], 
		inCart: false,
		inCarousel: false,
		count: 0,
		total: 0
	},
	{
		id: 15,
		title: "Couple",
		img: "img/couple2.jpg",
		price: 100,
		artist: "MDB",
		year: 2018,
		info:
			"Lorem ipsum dolor amet offal butcher quinoa sustainable gastropub, echo park actually green juice sriracha paleo. Brooklyn sriracha semiotics, DIY coloring book mixtape craft beer sartorial hella blue bottle. Tote bag wolf authentic try-hard put a bird on it mumblecore. Unicorn lumbersexual master cleanse blog hella VHS, vaporware sartorial church-key cardigan single-origin coffee lo-fi organic asymmetrical. Taxidermy semiotics celiac stumptown scenester normcore, ethical helvetica photo booth gentrify.",
		tags: ["deco", "rust", "black", "MDB"], 
		inCart: false,
		inCarousel: false,
		count: 0,
		total: 0
	},
];

export const detailProduct = {
	id: 7,
	title: "Autunm",
	img: "img/autunm_candle.jpg",
	img_carousel: "img/autunm_candle_night.jpg",
	price: 30,
	artist: "MOI",
	year: 2018,
	info:
		"Lorem ipsum dolor amet offal butcher quinoa sustainable gastropub, echo park actually green juice sriracha paleo. Brooklyn sriracha semiotics, DIY coloring book mixtape craft beer sartorial hella blue bottle. Tote bag wolf authentic try-hard put a bird on it mumblecore. Unicorn lumbersexual master cleanse blog hella VHS, vaporware sartorial church-key cardigan single-origin coffee lo-fi organic asymmetrical. Taxidermy semiotics celiac stumptown scenester normcore, ethical helvetica photo booth gentrify.",
	tags: ["candle", "deco", "white", "orange", "MOI"], 
	inCart: false,
	inCarousel: true,
	count: 0,
	total: 0
};

