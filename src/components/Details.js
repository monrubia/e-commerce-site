import React, { Component } from 'react';
import {ProductConsumer} from '../Context';
import {Link} from 'react-router-dom';
import {ButtonContainer} from './styledComponents/Button';

export default class Details extends Component {
    render() {
        return (
            <ProductConsumer>
                {value =>{
                    const {id, img, info, price, title, artist, year, inCart} = value.detailProduct;
                    return (
                        <div className="container py-5">
                            <div className="row">
                                <div className="col-10 mx-auto text-center text-slanted text-blue my-5">
                                    <h1 className="text-capitalize">{title}</h1>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-10 mx-auto col-md-6 my-3 text-capitalize">
                                    <img src={img} className="img-fluid" alt={title}/>
                                </div>
                                <div className="col-10 mx-auto col-md-6 my-3 ">
                                    <h2 className="text-capitalize">Product: {title}</h2>
                                    <h3 className="text-capitalize d-flex justify-content-between">
                                        <span>Artist:  <strong>{artist}</strong></span>
                                        <span>Year: {year}</span>
                                    </h3>
                                    <h4 className="text-capitalize">price: {price} <span>€</span></h4>
                                    <p className="text-capitalize font-weight-bold mt-3 mb-0">
                                        Information:
                                    </p>
                                    <p className="text-muted">
                                        {info}
                                    </p>
                                    <div>
                                        <Link to='/products'>
                                            <ButtonContainer>
                                                Back to products
                                            </ButtonContainer>
                                        </Link>
                                        <ButtonContainer
                                            cart
                                            disable={inCart}
                                            onClick={() =>{
                                                value.addToCart(id);
                                                value.openModal(id);
                                        }}>
                                            {inCart ? "in Cart" : "add to cart"}    
                                        </ButtonContainer>
                                    </div>
                                </div>
                            </div>
                        </div>
                    )
                }}
            </ProductConsumer>
        )
    }
}
