import React from "react";
import Button from 'react-bootstrap/Button';


export default function BackToTop() {

    const handleClick = () => { 
        window.scrollTo(0, 0) 
    }

    return (
        <div className="d-flex flex-row-reverse mr-3 mb-3">
            <Button
                variant="light"
                onClick={handleClick}
            >
                <i class="fas fa-chevron-up"></i>
            </Button>
        </div>
    )
}