export const colourOptions = [
    { value: 'white', label: 'White', color: '#000000'},
    { value: 'vanilla', label: 'Vanilla', color: '#f5f5ce'},
    { value: 'yellow', label: 'Yellow', color: '#FFC400'},
    { value: 'orange', label: 'Orange', color: '#FF8B00' },
    { value: 'red', label: 'Red', color: '#FF5630', isDisabled: true},
    { value: 'pink', label: 'Pink', color: '#ff7af4', isDisabled: true},
    { value: 'purple', label: 'Purple', color: '#a948c7' },
    { value: 'ocean', label: 'Ocean', color: '#00B8D9'}, 
    { value: 'blue', label: 'Blue', color: '#0052CC' },
    { value: 'green', label: 'Green', color: '#36B37E' },
    { value: 'rust', label: 'Rust', color: '#B7410E' },
    { value: 'black', label: 'Black', color: '#253858' },
    { value: 'silver', label: 'Silver', color: '#666666', isDisabled: true}
];

export const authorOptions =[
    { value: '', label: 'all'},
    { value: 'MDB', label: 'MDB'},
    { value: 'Emma', label: 'Emma'},
    { value: 'Marta', label: 'Marta'},
    { value: 'MOI', label: 'MOI'}
];

export const typeOptions =[
    { value: '', label: 'all'},
    { value: 'deco', label: 'decoration'},
    { value: 'candle', label: 'candle'},
    { value: 'bowl', label: 'bowl'},
    { value: 'mug', label: 'mug'},
    { value: 'dish', label: 'dish'},
    { value: 'kitchen', label: 'kitchen'}
];

/*
export const allColors = function getAllColors() {
    const allColors = [];
    colourOptions.forEach(element => {
        allColors.push(element.value);
    });
    console.log(allColors);
    return allColors
}
*/
  