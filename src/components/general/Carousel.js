import React, { Component, useContext } from 'react';
import Carousel from 'react-bootstrap/Carousel';
import { Link } from 'react-router-dom';
import { ProductContext } from '../../Context';

function GetValues() {
    const value = useContext(ProductContext);
    return (
        <React.Fragment>
            <Carousel>
                {value.products.length !== 0 ? (
                    value.products.map(product => {
                        if (product.inCarousel === true){
                        const { id, img, img_carousel, title } = product;
                        return (
                            <Carousel.Item key={id}>
                            <div className="img-carousel-container">
                                <Link to='/details' onClick={() => { value.handleDetail(id) }}>
                                    <img
                                        className="d-block w-100"
                                        src={img_carousel ? img_carousel : img}
                                        alt={title}
                                    />
                                </Link>
                            </div>
                        </Carousel.Item>
                        )}
                    })
                ) : ('Loading')
                }
            </Carousel>
        </React.Fragment>
    )
}


export default class CarouselHP extends Component {
    render() {
        //console.log(`inside CarouselHP, calling BasicCarousel`);
        return (
            <React.Fragment>
                <GetValues />
            </React.Fragment>
        )
    }
}
