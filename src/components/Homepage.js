import React, { Component } from 'react';
import CarouselHP from './general/Carousel';
import { Link } from 'react-router-dom';
import { ProductConsumer } from '../Context';
import logo from '../logo_AMDB.png';

export default class Homepage extends Component {
    render() {
        return (
            <ProductConsumer>
                {value => {
                    const { modalOpen, closeModal } = value;
                    if (modalOpen) {
                        closeModal();
                    }
                    return (
                        <React.Fragment>
                            <div className="homepage carrosel">
                                <CarouselHP />
                            </div>
                            <div className="container mx-auto mb-5">
                                <div className="row">
                                    <div className="col-8 col-md-6 mx-auto my-4 homepage logo">
                                        <Link to='/products' >
                                            <img src={logo} alt="group logo" className="img-fluid" />
                                        </Link>
                                    </div>
                                </div>
                            </div>
                        </React.Fragment>
                    )
                }}
            </ProductConsumer>
        )
    }
}
