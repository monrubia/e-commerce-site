import React, { Component } from 'react';
import Select from 'react-select';

/** Own components / data */
import { ProductConsumer } from '../Context';
import ProductDetail from './ProductDetail';
import { colourStyles } from './general/colourStyles';
import { colourOptions, authorOptions, typeOptions } from './general/colourOptions';
import BackToTop from './general/BackToTop';


const selectIfHasOneTag = (product, arrayOfFilters) => {
    return ((product.tags.filter(tag => arrayOfFilters.includes(tag))).length > 0)
}

// function to determine array of products to be returned.
// b,c,d=filters: collection of tags stored on state.
const filteredProducts = (arrayOfProducts, arrayOfFilters, c, d) => {
    if (arrayOfFilters.length > 0) {
        // each filter will add products to show. 
        // returns array of products containing at least one of the filtering tags (stored on b)
        arrayOfProducts = arrayOfProducts.filter(product => selectIfHasOneTag(product, arrayOfFilters));
    }
    //c,d =this.state.author/type.
    if (c) {
        arrayOfProducts = arrayOfProducts.filter(product => product.tags.includes(c));
    }
    if (d) {
        arrayOfProducts = arrayOfProducts.filter(product => product.tags.includes(d));
    }
    return arrayOfProducts
};

export default class Filtering extends Component {
    constructor(props) {
        super(props);
        this.state = {
            colorSelection: [],
            authorSelection: '',
            typeSelection: ''
        };
        this.handleChangeColor = this.handleChangeColor.bind(this);
        this.handleChangeAuthor = this.handleChangeAuthor.bind(this);
        this.handleChangeType = this.handleChangeType.bind(this);
    }

    handleChangeColor(selected) {
        let filtersToPass = [];
        if (selected !== null) {
            for (const tag of Object.values(selected)) {
                filtersToPass.push(tag.value);
            }
        }
        this.setState({
            colorSelection: filtersToPass,
        });
    }
    handleChangeAuthor(selected) {
        this.setState({
            authorSelection: selected.value,
        });
    }
    handleChangeType(selected) {
        this.setState({
            typeSelection: selected.value,
        });
    }

    render() {
        return (
            <React.Fragment>
                <div className="container top-padding">
                    <div className="row filtering justify-content-center">
                        <div className="col-10 col-md-6 mb-2 color-selector">
                            <Select
                                closeMenuOnSelect={true}
                                isMulti
                                options={colourOptions}
                                styles={colourStyles}
                                onChange={this.handleChangeColor}
                            />
                        </div>
                        <div className="col-10 col-md-3 mb-2 author-selector">
                            <Select
                                closeMenuOnSelect={true}
                                options={authorOptions}
                                name="authorSelection"
                                onChange={this.handleChangeAuthor}
                            />
                        </div>
                        <div className="col-10 col-md-3 mb-2 type-selector">
                            <Select
                                closeMenuOnSelect={true}
                                options={typeOptions}
                                name="typeSelection"
                                onChange={this.handleChangeType}
                            />
                        </div>

                    </div>
                    <div className="row plp">
                        <ProductConsumer>
                            {value => {
                                const { modalOpen, closeModal } = value;
                                if (modalOpen) {
                                    closeModal();
                                }
                                return filteredProducts(value.products, this.state.colorSelection.flat(), this.state.authorSelection, this.state.typeSelection).map(product => {
                                    return <ProductDetail
                                        key={product.id}
                                        product={product}
                                        value={value}
                                    />;
                                })
                            }}
                        </ProductConsumer>
                    </div>
                </div>
                <BackToTop />
            </React.Fragment>
        )
    }
}
