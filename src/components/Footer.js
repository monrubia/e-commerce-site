import React, { Component } from 'react';
import { Link } from 'react-router-dom';


export default class Footer extends Component {
    render() {
        return (
            <div className="footer">
                <div className="container pt-4">
                    <div className="row">
                        <div className="col-md-5 mx-auto text-center footer-info">
                            <ul>
                                <ul><Link to="/contactus"> The group </Link></ul>
                                <ul><Link to="/contactus"> Contact </Link></ul>
                                <ul>Returning policies  </ul>
                                <ul>Conditions          </ul>
                            </ul>
                        </div>
                        <div className="col-md-5 mx-auto text-center footer-info">
                            <ul>
                                <ul>Legal mentions          </ul>
                                <ul>Data protection         </ul>
                                <ul>Follow us: 
                                    <span> <i className="fab fa-facebook-square"></i></span>
                                    <span> <i className="fab fa-twitter-square"></i></span>
                                    <span> <i className="fab fa-instagram-square"></i></span>
                                    <span> <a href="https://www.linkedin.com/in/miriam-onrubia-iba%C3%B1ez/"><i className="fab fa-linkedin"></i></a> </span>
                                    <span> <a href="https://gitlab.com/monrubia/e-commerce-site"><i className="fab fa-gitlab"></i></a></span>
                                </ul>
                            </ul>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-10 mx-auto mb-3 text-center footer-socialmedia">
                            <p> © 2020 Miriam Onrubia</p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
