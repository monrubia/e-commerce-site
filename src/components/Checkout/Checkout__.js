import React, { useEffect } from 'react';
import { Elements } from '@stripe/react-stripe-js';
import { loadStripe } from "@stripe/stripe-js";
import CheckoutForm from './CheckoutForm2';
import { ProductConsumer } from '../Context';

//const stripePromise = "pk_test_51H5bXrIznmMp27vT6SerYQoWsxzqdsXQel6B5aat5l86fDYqAa8bXPprgyqivRu6vsPCadqmOyi2n8lzWYMxdibg00HRdIP5Gg";
const stripePromise = loadStripe("pk_test_51H5bXrIznmMp27vT6SerYQoWsxzqdsXQel6B5aat5l86fDYqAa8bXPprgyqivRu6vsPCadqmOyi2n8lzWYMxdibg00HRdIP5Gg");

const Checkout = ({ history }) => {
	useEffect(() => {
		window.scrollTo(0, 0)
	}, [])

	return (
		<ProductConsumer>
			{value => {
				console.log(`Checkout component... Value.cart: ${value.cart}`);
				return (
					<Elements stripe={stripePromise}>
						<CheckoutForm
							stripe={stripePromise}
							checkout={value.cart}
							cartTotal={value.cartTotal}
							history={history} />
					</Elements>
				)
			}}
		</ProductConsumer>
	)
}

export default Checkout;