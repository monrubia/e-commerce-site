import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import {
    CardNumberElement,
    CardExpiryElement,
    CardCVCElement,
    injectStripe
} from 'react-stripe-elements';
//import Receipt from '../Receipt';
import axios from 'axios';
import Receipt from './Receipt';
import './CheckoutForm.css'

const CheckoutForm = ({ checkout, cartTotal, stripe, history }) => {
    if (checkout.length === null) {
        history.push('/')
    }

    const [receiptUrl, setReceiptUrl] = useState('')

    const handleSubmit = async event => {
        event.preventDefault()

        const { token } = await stripe.createToken()

        const order = await axios.post('http://localhost:7000/api/stripe/charge', {
            checkout: checkout,
            amount: cartTotal.toString().replace('.', ''),
            source: token.id,
            receipt_email: 'customer@example.com'
        })
        setReceiptUrl(order.data.charge)
    }
    if (receiptUrl) {
        console.log(`In checkout: ${receiptUrl.amount}`);
        return (
            <div className="col-12 success">
                <h2>Payment Successful!</h2>
                <Link to="/">Home</Link>
                <Receipt receiptUrl={receiptUrl} checkout={checkout}/>
            </div>
        )
    }
    return (
        <div className="checkout-form">
            <p>Amount: {cartTotal}€</p>
            <form onSubmit={handleSubmit}>
                <label>
                    Card details
                    <CardNumberElement />
                </label>
                <label>
                    Expiration date
                    <CardExpiryElement />
                </label>
                <label>
                    CVC
                    <CardCVCElement />
                </label>
                <button type="submit" className="order-button">
                    Pay
            </button>
            </form>
        </div>
    )
}

export default injectStripe(CheckoutForm)