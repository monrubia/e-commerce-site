import React, { useEffect } from 'react';
import { StripeProvider, Elements } from 'react-stripe-elements';
import CheckoutForm from './CheckoutForm';
import { ProductConsumer } from '../../Context';

const Checkout = ({ history }) => {
	useEffect(() => {
		window.scrollTo(0, 0)
	}, [])

	return (
		<div className="container">
			<div className="row">
				<div className="col-10 d-flex justify-content-center mx-auto pt-5 checkout">
					<div className="text-danger no-paymnet-possible">
						<p>This site is not made to sell any product, it is only a portfolio site.</p>
						<p>The payment will not be made and the items have not been bought. </p>
					</div>
				</div>
				<div className="col-12 d-flex justify-content-center mx-auto pb-5 checkout">
					<StripeProvider apiKey="pk_test_51H5bXrIznmMp27vT6SerYQoWsxzqdsXQel6B5aat5l86fDYqAa8bXPprgyqivRu6vsPCadqmOyi2n8lzWYMxdibg00HRdIP5Gg">
						<Elements>
							<ProductConsumer>
								{value => {
									return <CheckoutForm
										checkout={value.cart}
										cartTotal={value.cartTotal}
										history={history} />
								}}
							</ProductConsumer>
						</Elements>
					</StripeProvider>
				</div>
			</div>
		</div>
	)
}

export default Checkout