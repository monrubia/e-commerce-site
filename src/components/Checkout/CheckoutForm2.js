import React, { useState, useEffect } from 'react'
import {
    CardElement,
    //useStripe,
    useElements,
} from "@stripe/react-stripe-js";
import './CheckoutForm_stripe.css';

export default function CheckoutForm({ checkout, cartTotal, stripe, history }) {
    console.log(`checkout: ${typeof (checkout)} ---- cartTotal: ${cartTotal} `);
    
    const [succeeded, setSucceeded] = useState(false);
    const [error, setError] = useState(null);
    const [processing, setProcessing] = useState('');
    const [disabled, setDisabled] = useState(true);
    const [clientSecret, setClientSecret] = useState('');
    //const stripe = useStripe();
    const elements = useElements();
    useEffect(() => {
        const { token } = stripe.createToken('pii', {
            personal_id_number: '123456789'
        })
        // Create PaymentIntent as soon as the page loads
        window
            .fetch("/create-payment-intent", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({ items: checkout, source: token.id })
            })
            .then(res => {
                console.log(`checkoutForm2 response from fetch: ${res.json}`);
                return res.json();
            })
            .then(data => {
                console.log(`Data response to fetch: ${data}`);
                setClientSecret(data.clientSecret);
                console.log(data.clientSecret);
            });
    }, []);
    const cardStyle = {
        style: {
            base: {
                color: "#32325d",
                fontFamily: 'Arial, sans-serif',
                fontSmoothing: "antialiased",
                fontSize: "16px",
                "::placeholder": {
                    color: "#32325d"
                }
            },
            invalid: {
                color: "#fa755a",
                iconColor: "#fa755a"
            }
        }
    };
    const handleChange = async (event) => {
        // Listen for changes in the CardElement
        // and display any errors as the customer types their card details
        setDisabled(event.empty);
        setError(event.error ? event.error.message : "");
    };
    const handleSubmit = async ev => {
        ev.preventDefault();
        setProcessing(true);
        const payload = await stripe.confirmCardPayment(clientSecret, {
            payment_method: {
                card: elements.getElement(CardElement),
                billing_details: {
                    name: ev.target.name.value
                }
            }
        });
        if (payload.error) {
            setError(`Payment failed ${payload.error.message}`);
            setProcessing(false);
        } else {
            setError(null);
            setProcessing(false);
            setSucceeded(true);
        }
    };
    return (
        <React.Fragment>
            <div className="amoutToPay text-center mb-5 mt-5">
                To be paid: {cartTotal}€
            </div>
            <div className="form d-flex justify-content-center">
                <form id="payment-form" onSubmit={handleSubmit}>
                    <CardElement id="card-element" options={cardStyle} onChange={handleChange} />
                    <button
                        className="btn_checkout"
                        disabled={processing || disabled || succeeded}
                        id="submit"
                    >
                        <span id="button-text">
                            {processing ? (
                                <div className="spinner" id="spinner"></div>
                            ) : (
                                    "Pay"
                                )}
                        </span>
                    </button>
                    {/* Show any error that happens when processing the payment */}
                    {error && (
                        <div className="card-error" role="alert">
                            {error}
                        </div>
                    )}
                    {/* Show a success message upon completion */}
                    <p className={succeeded ? "result-message" : "result-message hidden"}>
                        Payment succeeded, see the result in your
                        <a href={`https://dashboard.stripe.com/test/payments`}> {" "} Stripe dashboard.</a> 
                        Refresh the page to pay again.
                    </p>
                </form>
            </div>
        </React.Fragment>
    );
}