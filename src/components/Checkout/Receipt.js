import React from 'react';

//export default class Receipt extends Component {
const Receipt = ({ receiptUrl, checkout }) => {
    return (
        <React.Fragment>
            <div className="container border border-dark">
                <div className="row border mz-2 my-3">
                    <div className="ml-4 receipt-number">
                        Receipt nº: {receiptUrl.created}
                    </div>
                </div>
                <div className="row justify-content-between">
                    <div className="col-3 text-left border rounded ml-4 atelier-address">
                        <span className="text-brand">Atelier</span>
                        <p>Address:</p>
                        <span>42 rue quelqueplace </span>
                        <span>75000 Paris</span>
                    </div>
                    <div className="col-3 text-right border rounded mr-4 billing-details">
                        <p>
                            <span> {receiptUrl.billing_details.name ? receiptUrl.billing_details.name : 'Mme. Name Surname'}                </span>
                            <p> Email: myfirstemail@mail.com {receiptUrl.billing_details.email}, {receiptUrl.billing_details.receipt_email}   </p>
                            <span> Tel: {receiptUrl.billing_details.phone ? receiptUrl.billing_details.phone : '0612345678'}                </span>
                        </p>

                        <p>Address:</p>
                        <span> {receiptUrl.billing_details.address.line1 ? receiptUrl.billing_details.address.line1 : '5 rue Le Corbusier'} </span>
                        <span> {receiptUrl.billing_details.address.line2 ? receiptUrl.billing_details.address.line2 : ''} </span>
                        <span> {receiptUrl.billing_details.postal_code ? receiptUrl.billing_details.postal_code : '75000'} {' '} {receiptUrl.billing_details.city ? receiptUrl.billing_details.city : 'Paris'}</span>
                    </div>
                </div>

                <div className="row pt-5">
                    <div className="col-12 receipt">
                        <table className="table table-sum-up">
                            <thead className="border-top border-bottom">
                                <td>Description</td>
                                <td>Item price (€)</td>
                                <td>Item amount</td>
                                <td>€</td>
                            </thead>
                            <tbody>
                                {checkout.map(item =>
                                    <tr>
                                        <td> {item.title} </td>
                                        <td> {item.price} </td>
                                        <td> {item.count} </td>
                                        <td> {item.total}  </td>
                                    </tr>
                                )}
                            </tbody>
                            <tfoot className="border-top border-bottom">
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td>Total</td>
                                    <td><strong>{receiptUrl.amount / 100}</strong></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}

export default Receipt;