import React, { Component } from 'react';
import logo from '../logo_AMDB.png';

export default class Contactus extends Component {

    render() {
        return (
            <React.Fragment>
            <div className="container mx-auto mb-5 top-padding">
                <div className="row">
                    <div className="col-8 col-md-6 mx-auto my-4 contactus logo">
                        <img src={logo} alt="group logo" className="img-fluid" />
                    </div>
                </div>
            </div>
            <div className="col-10 col-md-8 mx-auto py-3 text-align contactus">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam euismod quam non sapien dignissim, ut blandit velit lacinia. Vivamus bibendum mauris nec sapien ultricies facilisis. Proin at sollicitudin felis, ac accumsan mi. Suspendisse feugiat leo a congue faucibus. Nullam faucibus tortor lorem. Etiam sit amet laoreet eros, ac finibus arcu. Phasellus tempor vehicula ex in laoreet. Donec vitae nisl vulputate, ultrices massa at, vestibulum nisl. Aliquam nisi velit, feugiat quis ligula quis, auctor efficitur lectus.</p>
                <p>Nam sit amet eros porta, condimentum velit et, semper dolor. Nulla posuere libero sit amet dolor consectetur, at efficitur nibh aliquet. Aliquam pretium ex eu lorem molestie, in interdum diam fermentum. Maecenas porttitor ullamcorper euismod. Curabitur condimentum tempus egestas. Praesent eu erat vitae mi mattis porttitor aliquam eget nisi. Pellentesque rhoncus eleifend nisl quis egestas. In hendrerit tristique est non interdum. Curabitur pulvinar sapien eu leo congue facilisis. Proin a mi nibh. Suspendisse potenti. Vivamus pulvinar tellus sem, eu egestas risus pretium consectetur. Praesent ut dignissim nisi.</p>
                <p>Maecenas vel ultrices arcu. Quisque quam erat, fringilla volutpat pretium at, ullamcorper ut elit. In hac habitasse platea dictumst. Phasellus rhoncus leo nec lacus fermentum vehicula. Phasellus dolor tortor, molestie vel nisi eu, tempus tincidunt nunc. Etiam ornare tortor ut nisl convallis, eget tincidunt magna interdum. Maecenas ex ante, posuere ut enim sed, fermentum finibus tortor. Phasellus elementum bibendum lectus, nec consectetur lacus mollis eget.</p>
            </div>
        </React.Fragment>
        )
    }
}
