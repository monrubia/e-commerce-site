import React, { Component } from 'react'

export default class Unfinished extends Component {

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-10 mx-auto text-center text-title pt-5 top-padding">
                        <h3>working on that page</h3>
                        <h4>the URL <span className="text-info">{this.props.location.pathname}</span> does not exist yet</h4>
                    </div>
                    
                </div>
            </div>
        )
    }
}
