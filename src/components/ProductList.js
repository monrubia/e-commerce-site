import React, { Component } from 'react';
import {ProductConsumer} from '../Context';
import ProductDetail from './ProductDetail';

export default class ProductList extends Component {
    render() {
        return (
            <React.Fragment>
                    <div className="container top-padding">
                        <div className="row">
                            <ProductConsumer>
                                {value => {
                                    const { modalOpen, closeModal } = value;
                                    if (modalOpen){
                                        closeModal();
                                    }
                                    return value.products.map(product => {
                                        return <ProductDetail 
                                            key={product.id} 
                                            product={product}
                                            value={value}
                                        />;
                                    });
                                }}
                            </ProductConsumer>
                        </div>
                    </div>

            </React.Fragment>
        )
    }
}
