import React, { Component } from 'react'

export default class Default extends Component {

    render() {
        console.log(this.props);
        return (
            <div className="container">
                <div className="row">
                    <div className="col-10 mx-auto text-center text-title top-padding">
                        <h3 className="display-3">404 error</h3>
                        <h3>page not found</h3>
                        <h4>the URL <span className="text-info">{this.props.location.pathname}</span> does not exist</h4>
                    </div>
                    
                </div>
            </div>
        )
    }
}
