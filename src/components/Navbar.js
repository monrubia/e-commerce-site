import React, { Component } from 'react';
import {
	MDBNavbar, MDBNavbarBrand, MDBNavbarNav, MDBNavItem,
	MDBNavLink, MDBNavbarToggler, MDBCollapse, MDBIcon,
	MDBDropdownToggle, MDBDropdown, MDBDropdownMenu, MDBDropdownItem
} from "mdbreact";


export default class Header extends Component {
	state = {
		isOpen: false, 
		background: 'no-background'
	};

	toggleCollapse = () => {
		this.setState({ 
			isOpen: !this.state.isOpen, 
			background: 'need-background'
		});
	}

	listenScrollEvent = e => {
		if (window.scrollY > 20) {
			this.setState({background: 'need-background'})
		} else {
			this.setState({background: 'no-background'})
		}
	}

	componentDidMount() {
		window.addEventListener('scroll', this.listenScrollEvent)
	}

	render() {
		return (
			<React.Fragment>
				<MDBNavbar light expand="md" fixed="top" className={this.state.background}>
					<MDBNavbarBrand href="/">
					<span className="text-brand pl-5">Atelier</span>
					</MDBNavbarBrand>
					<MDBNavbarToggler onClick={this.toggleCollapse} />
					<MDBCollapse id="navbarCollapse3" isOpen={this.state.isOpen} navbar>
						<MDBNavbarNav right>
							<MDBNavItem>
								<MDBNavLink to="/products">
								<MDBIcon icon="store-alt" />
								</MDBNavLink>
							</MDBNavItem>
							<MDBNavItem>
								<MDBDropdown>
									<MDBDropdownToggle nav caret>
										<MDBIcon icon="user" />
									</MDBDropdownToggle>
									<MDBDropdownMenu className="dropdown-default">
										<MDBDropdownItem href="#!">Logging</MDBDropdownItem>
										<MDBDropdownItem href="#!">Logout</MDBDropdownItem>
									</MDBDropdownMenu>
								</MDBDropdown>
							</MDBNavItem>
							<MDBNavItem>
								<MDBNavLink to="/cart">
									<MDBIcon icon="shopping-cart" />
								</MDBNavLink>
							</MDBNavItem>
						</MDBNavbarNav>
					</MDBCollapse>
				</MDBNavbar>
			</React.Fragment>
		)
	}
}

