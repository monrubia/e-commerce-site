import React from 'react';

export default function CartColumns() {
    return (
        <div className="container-fluid text-center d-none d-lg-block">
            <div className="row">
                <div className="col-10 mx-auto col-lg-2">
                    <div className="p text-uppercase">
                        products
                    </div>
                </div>
                <div className="col-10 mx-auto col-lg-2">
                    <div className="p text-uppercase">
                        name of products
                    </div>
                </div>
                <div className="col-10 mx-auto col-lg-2">
                    <div className="p text-uppercase">
                        price
                    </div>
                </div>
                <div className="col-10 mx-auto col-lg-2">
                    <div className="p text-uppercase">
                        quantity
                    </div>
                </div>
                <div className="col-10 mx-auto col-lg-2">
                    <div className="p text-uppercase">
                        remove
                    </div>
                </div>
                <div className="col-10 mx-auto col-lg-2">
                    <div className="p text-uppercase">
                        total
                    </div>
                </div>
            </div>
        </div>
    )
}

