/** Import general components */
import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { createBrowserHistory } from 'history';

/** Import site components */
import Header from './components/Navbar';
import Homepage from './components/Homepage';
import Filtering from './components/Filtering';
import Details from './components/Details';
import Modal from './components/Modal';
import Cart from './components/Cart';
import Checkout from './components/Checkout';
import Receipt from './components/Checkout';
import Unfinished from './components/Unfinished';
import Contactus from './components/Contactus';
import Default from './components/Default';
import Footer from './components/Footer';

/** Import style */
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

const history = createBrowserHistory();

const App = () => {
	return (
		<React.Fragment>
			<Header />
			<Switch>
				<Route exact path="/" component={Homepage} />
				<Route path="/products" component={Filtering} />
				<Route path="/details" component={Details} />
				<Route path="/cart" component={Cart} />
				<Route path="/checkout" 
					render={()=>(
						<Checkout 
							history = {history}
						/>
					)}
				/>
				<Route path="/logging" component={Unfinished} />
				<Route path="/contactus" component={Contactus} />
				<Route path="/receipt" component={Receipt}/>
				<Route component={Default} />
			</Switch>
			<Modal />
			<Footer />
		</React.Fragment>
	);
}

export default App;
