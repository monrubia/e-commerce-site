/** ATENTION!!! NOT READY FOR PRODUCTION */
require('dotenv').config()

const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);

async function postCharge2(req, res) {
	try {
		const { checkout, amount, source, receipt_email } = req.body
		//console.log(`my request--- checkout: ${checkout}, amount: ${amount}, source: ${source}, receipt_email: ${receipt_email}`)

		//calculate the total to pay:
		function calculateOrderAmount(arrayOfItems){
    		let totalAmountToPay = 0;
    		arrayOfItems.map(item => (totalAmountToPay += item.total));
    		//console.log(`calculated total to pay: ${totalAmountToPay}`);
    		return totalAmountToPay;
		};

		// get a list of bought items:
		function getAllItemsTitle(arrayOfItems){
			let listOfItems = '';
			arrayOfItems.map(item => (
				listOfItems = listOfItems + item.title + ", "
			));
			console.log(`get list of bought items: ${listOfItems}`);
    		return listOfItems;
		};

		const totalToPay = calculateOrderAmount(checkout)*100;
		const listOfitems = getAllItemsTitle(checkout);

		stripe.charges.create(
			{
				amount: totalToPay,
				currency: 'eur',
				source: source,
				description: listOfitems
			},
			function (err, charge) {
				if (!charge) {
					throw new Error(`charge unsuccessful, error: ${err}`)
				}
				res.status(200).json({
					message: 'charge posted successfully',
					charge,
					checkout
				})
			}
		);
	} catch (error) {
		res.status(500).json({
			message: error.message
		})
	}
}

module.exports = postCharge2