/** Server part.Located root dir. 
 * Requires frontend: CheckoutForm2.js. 
*/
const express = require("express");
const app = express();
const { resolve } = require("path");
require('dotenv').config();

// This is your real test secret API key.
const stripe = require("stripe")(process.env.STRIPE_SECRET_KEY);
app.use(express.static("."));
app.use(express.json());

//calculate the total to pay:
function calculateOrderAmount(arrayOfItems){
    let totalAmountToPay = 0;
    arrayOfItems.map(item => (totalAmountToPay += item.total));
    console.log(`total to pay: ${totalAmountToPay}`);
    return totalAmountToPay;
};

app.post("/create-payment-intent", async (req, res) => {
  const { items } = req.body;
  console.log(`Items to pay received on the server: ${items}`);
  // Create a PaymentIntent with the order amount and currency
  const paymentIntent = await stripe.paymentIntents.create({
    amount: calculateOrderAmount(items),
    currency: "euro"
  });
  console.log(`info for clientSecret: ${paymentIntent}`);
  res.send({
    clientSecret: paymentIntent.client_secret
  });
  console.log(paymentIntent.client_secret);
});
app.listen(4242, () => console.log('Node server listening on port 4242!'));