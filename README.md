# e-commerce-site

E-commerce site build using React.js (front-end), Node.js-Express (back-end), Stripe (Merchant part), Netlify (CI/CD).
The site can be found at: https://ateliermdb.netlify.app/